# IdentityManager

An identity manager for a to be selected blockchain. The initial goal is to produce an ephemeral link as a result of an identity registration. A crypto fee in the selected blockchain will be used to fund the ecosystem.